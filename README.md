# sky_challenge_code v0.2

This is the challenge code for network automation.
This project is based on Flask_restful, netmiko, ncclient with python language.

It is a REST_API app that you can get all interfaces states, create new loopback, and delete loopback on network devices (cisco ios xe)

For interface information it uses Netmiko on background. If you want to create or delete a loopback interface backend will use ncclient with Netconf. Also, this tool provides "Dry-Run" feature and you can see what payload will be sent to the device without actualy run the config on it. 

## Instruction

Device credentials and Flask secret_key is based on .env_template format. Please fill your ".env" file based on this [template](https://gitlab.com/VahidTa/sky_challenge_code/-/tree/main/.env_template).

In this [postman collection](https://gitlab.com/VahidTa/sky_challenge_code/-/tree/main/src/templates) you can find related api calls.

### run app on machine

- Install python packages with ```pip install -r requirements.txt```
- execute ```python app.py```


### run app on docker

There is two way to implement the code on docker:

1. With single container:
    - Make sure you are in root of the project.
    - ```docker build -t awsome_network_api```
    - ```docker run -p 80:80 -d -v $PWD/logs:/var/www/logs --name auto_net awsome_network_api```
2. With docker-compose and two containers:
    - Make sure you are in root of the project.
    - ```docker-compose up -d```


## Monitor

You can find logs under [logs path](https://gitlab.com/VahidTa/sky_challenge_code/-/tree/main/logs) to trace the app.