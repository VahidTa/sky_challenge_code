FROM python:3.8

ENV APP_HOME=/var/www
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

COPY . $APP_HOME
WORKDIR $APP_HOME

RUN pip install -r $APP_HOME/requirements.txt