from datetime import datetime


def timer():
    current_time = datetime.strftime(
        datetime.now(),
        "%Y-%m-%d %H:%M:%S",
    )
    return current_time
