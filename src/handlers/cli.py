import logging

from netmiko import ConnectHandler
from src.models.insert import insert_data
from src.utils.timer import timer

logger = logging.getLogger("app.cli")


class CLIHandler:
    def __init__(self, hostname: str, *args) -> None:
        """using netmiko it will connect to device through ssh

        Args:
            hostname (str): ip/hostname of the device
            args : ["device-username", "device-password", "platform", "port"]
        """
        self.hostname = hostname
        self.config = args[0]
        self.port = self.config.get("port")
        self.platform = self.config.get("platform")
        self.username = self.config.get("device-username")
        self.password = self.config.get("device-password")

    @property
    def _cli_handler(self):
        """creates ssh connection handler"""
        device_config = {
            "device_type": self.platform,
            "host": self.hostname,
            "username": self.username,
            "password": self.password,
            "port": self.port,
        }

        net_connect = ConnectHandler(**device_config)
        return net_connect

    def show_command(self):
        """sends show ip int br command to the device"""

        try:
            logger.info(f"connecting to <{self.hostname}> ...")
            net_connect = self._cli_handler
            command = "show ip int brief"
            sent_time = timer()

            logger.info(f"getting response from the <{self.hostname}>")
            output = net_connect.send_command(
                command,
                use_textfsm=True,
            )
            # insert data to db
            insert_data(
                {
                    "device": self.hostname,
                    "action": "get-state",
                    "connection": "cli/ssh",
                    "port": self.port,
                    "sent_time": sent_time,
                    "command": command,
                    "rcv_time": timer(),
                    "response": output,
                }
            )

            logger.info(f"disconnecting from <{self.hostname}>")
            net_connect.disconnect()

            return output
        except Exception as err:
            return str(err)
