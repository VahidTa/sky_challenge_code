import logging

from ncclient import manager, xml_
from jinja2 import Environment, FileSystemLoader

from src.models.insert import insert_data
from src.utils.timer import timer

logger = logging.getLogger("app.ncclient")


class NCCHandler:
    def __init__(self, hostname: str, *args) -> None:
        """initial variables plus hostname(address) of the device

        Args:
            hostname (str): address/name of the device
            args : ['device-username', 'device-password', 'platform', 'port', 'intf_name', 'ipv4', 'ipv4_mask', 'desc', 'delete']

        """

        self.hostname = hostname
        self.wrong_fields = list()
        self.config = args[0]
        self.port = self.config.get("port")
        self.platform = self.config.get("platform")
        self.username = self.config.get("device-username")
        self.password = self.config.get("device-password")

    @property
    def _nc_handler(self):
        """creates a connection to device

        Returns:
            Manager: device manager
        """
        mgr = manager.connect(
            host=self.hostname,
            port=self.port,
            username=self.username,
            password=self.password,
            hostkey_verify=False,
            device_params={"name": self.platform},
            timeout=10,
        )

        return mgr

    def _save_config(self, manager: manager) -> None:
        """save the config to device

        Args:
            manager (manager): ncclient manager
        """
        save_body = (
            "<cisco-ia:save-config xmlns:cisco-ia='http://cisco.com/yang/cisco-ia'/>"
        )
        manager.dispatch(xml_.to_ele(save_body))

    def _configi(self, data: dict) -> str:
        """generates config file with jinja2 templating

        Args:
            data (dict): parameters for interface configs (e.g., intf_name(e.g. 1), ipv4, ipv4_mask, desc)

        Returns:
            str: config that will sit on device
        """

        current_time = timer()
        template = "intf.xml"

        file_loader = FileSystemLoader("src/templates/netconf/")
        env = Environment(loader=file_loader)
        template = env.get_template(template)
        output = template.render(data=data, current_time=current_time)

        return output

    def edit_intf(self, switch: bool):
        """edits loopback interface on device based on parameters if 'delete' parameter exists, it will delete the interface and if 'delete' not exists it will config the interface.

        Args:
            switch (bool): checks dry-run feature

        Returns:
            [bool, Exception, str]: if task completed successfully it will "True", if there is an Exception returns it. else string.
        """

        intf_name = self.config.get("intf_name")

        try:
            if not intf_name.isdigit():
                logger.error(f"{intf_name}")
                return TypeError(
                    f"check <intf_name> parameter. interface name must be signed integer (e.g. 1) not <{intf_name}>"
                )
        except:
            return TypeError(f"payload is not proper: <{intf_name}>")

        logger.info(f"generating config ...")
        config = self._configi(self.config)

        if switch == "false":
            operation = "none" if self.config.get("delete") else "merge"
            logger.info(f"connecting to <{self.hostname}> ...")
            try:
                m = self._nc_handler
                logger.info(f"locking configuration on <{self.hostname}>")
                with m.locked(target="candidate"):
                    logger.info(
                        f"operation <{self.config.get('delete', 'config')}> on <{self.hostname}> for Loopbak<{intf_name}>"
                    )
                    response = m.edit_config(
                        config=config, default_operation=operation, target="candidate"
                    )
                    sent_time = timer()
                    m.commit()

                # insert data to db
                insert_data(
                    {
                        "device": self.hostname,
                        "action": f"{self.config.get('delete', 'config')}",
                        "dry-run": switch,
                        "connection": "netconf",
                        "port": self.port,
                        "sent_time": sent_time,
                        "command": config,
                        "rcv_time": timer(),
                        "response": f"{response}",
                    }
                )

                logger.info(f"saving config on <{self.hostname}>")
                self._save_config(m)
                logger.info(f"clossing the session ...")
                m.close_session()
                return True
            except Exception as err:
                return err
        # insert data to db
        insert_data(
            {
                "device": self.hostname,
                "action": f"{self.config.get('delete', 'config')}",
                "dry-run": switch,
                "connection": "netconf",
                "port": self.port,
                "time": timer(),
                "command": config,
            }
        )
        return config
