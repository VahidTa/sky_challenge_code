from src.handlers.db import mongodb_client


def retrive_data(host):
    """retrive data for a single host"""
    try:
        return mongodb_client.db.appTransactions.find({"device": host}, {"_id": 0})
    except Exception as err:
        return err