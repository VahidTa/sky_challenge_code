from src.handlers.db import mongodb_client


def insert_data(*data):
    """inserts data into mongoDB"""
    try:
        return mongodb_client.db.appTransactions.insert_one({**data[0]})
    except Exception as err:
        return err