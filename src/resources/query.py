import json
import logging

from bson.json_util import dumps
from flask_restful import Resource

from src.models.retrive import retrive_data
from src.serializers.query_serial import query_serializer

logger = logging.getLogger("app.query")


class Query(Resource):
    def get(self):
        """query to mongodb to find info of device
        ---
        parameters:
        - name: hostname
            in: query
            type: string
            required: true
            description: device ip/hostname
        responses:
        200:
            description: all records for given hostname
        400:
            description: when interface name is incorrect
        """
        data = query_serializer()
        hostname = data.get("hostname")
        try:
            logger.info(f'getting data from mongoDB for {hostname}')
            resp = retrive_data(hostname)
            info = dumps(resp)
            info = json.loads(info)

            logger.info(f'sending data back to user for {hostname}')
            return {"status": "status", "message": info}, 200
        except Exception as err:
            logger.error(f'something goes wrong {err}')
            return {"status": "failed", "message": f"{err}"}, 400
