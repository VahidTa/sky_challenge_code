import logging

from flask_restful import Resource
from src.handlers.cli import CLIHandler
from src.serializers.get_state_serial import get_state_serializer

logger = logging.getLogger("app.get_state")


class GetState(Resource):
    def get(self, hostname: str):
        """
        Gets all interfaces state from network device
        ---
        parameters:
        - name: hostname
            in: path
            type: string
            required: true
            description: device ip/hostname
        -name: device-username
            in: query
            type: string
            required: false
            description: username to connect with device
        -name: device-password
            in: query
            type: string
            required: false
            description: password to connect with device
        - name: port
            in: query
            type: string
            required: false
            description: device ssh port
        - name: platform
            in: query
            type: string
            required: false
            description: device platform
        responses:
        200:
            description: interfaces states
            properties:
                intf:
                type: string
                description: interface name

                status:
                type: string
                description: admin status of the interfaces

                proto:
                type: string
                description: protocol status of the interface
        401:
            description: if device authentication failed
        422:
            description: any exception that cannot be handles
        """

        data = get_state_serializer()

        cli = CLIHandler(hostname, data)
        logger.info(f"getting info from the <{hostname}> ... ")
        result = cli.show_command()

        if isinstance(result, list):
            for res in result:
                res.pop("ipaddr")
            logger.info(f"sending info back to user")
            return {"status": "success", "message": result}
        elif "Authentication to device failed" in result:
            logger.error(f"Authentication failed on {hostname}")
            return {
                "status": "failed",
                "message": f"{hostname} authentication failed. Please check .env file or ENV",
            }, 401
        logger.critical(f"{result}")
        return {"status": "failed", "message": f"{result}"}, 422
