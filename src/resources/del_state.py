import logging

from flask_restful import Resource
from flask import Response, session
from flask_jwt_extended import jwt_required
from src.handlers.netconf import NCCHandler
from src.serializers.del_config_serial import del_config_serializer

logger = logging.getLogger("app.del_config")


class DeleteConfig(Resource):
    @jwt_required()
    def delete(self, hostname: str):
        """
        Deletes loopback interface from the device
        ---
        parameters:
        - name: hostname
            in: path
            type: string
            required: true
            description: device ip/hostname
        -name: intf_name
            in: body
            type: string
            required: true
            description: loopback interface name
        -name: device-username
            in: body
            type: string
            required: false
            description: username to connect with device
        -name: device-password
            in: body
            type: string
            required: false
            description: password to connect with device
        - name: port
            in: body
            type: string
            required: false
            description: device ssh port
        - name: platform
            in: body
            type: string
            required: false
            description: device platform
        responses:
        204:
            description: if interface deleted succesfully
        400:
            description: when interface name is incorrect
        401:
            description: if device authentication failed
        404:
            description: when interface already deleted or not found on device.
        422:
            description: If socket is wrong .Any exception that cannot be handles
        """

        switch = session.get("dry-run", "false")

        data = del_config_serializer()
        data["delete"] = "delete"

        nc = NCCHandler(hostname, data)
        logger.info(f"deleting config on {hostname} ...")
        result = nc.edit_intf(switch)

        if isinstance(result, bool):
            logger.info(
                f'<Loopback{data.get("intf_name")}> deleted on {hostname}. sending info back to user'
            )
            return {
                "status": "suceess",
                "message": f'interface <Loopback{data.get("intf_name")}> deleted',
            }, 200
        elif isinstance(result, str):
            logger.info(f"sending info back to user with dry-run feature")
            return Response(result, mimetype="text/xml")
        elif "must be signed integer" in f"{result}":
            logger.error(f"interface name is incorrect -> {data}")
            return {"status": "failed", "message": f"{result}"}, 400
        elif "not open socket" in f"{result}":
            logger.error(f"port is not reachable on {hostname}")
            return {"status": "failed", "message": f"{result}"}, 422
        elif "AuthenticationException" in f"{result}":
            logger.error(f"authentication error for {hostname}")
            return {
                "status": "failed",
                "message": f"{hostname} authentication failed. Please check .env file parameters",
            }, 401
        elif "bad-element" in f"{result}":
            logger.error(
                f'interface <Loopback{data.get("intf_name")}> already deleted!'
            )
            return {
                "status": "failed",
                "message": f'interface <Loopback{data.get("intf_name")}> already deleted!',
            }, 404
        logger.critical(f"{result}")
        return {"status": "failed", "message": f"{result}"}, 400
