import logging

from flask_restful import Resource
from flask import Response, session
from flask_jwt_extended import jwt_required
from src.handlers.netconf import NCCHandler
from src.serializers.set_config_serial import set_config_serializer

logger = logging.getLogger("app.set_config")


class SetConfig(Resource):
    @jwt_required()
    def post(self, hostname: str):
        """
        Creates and configures the loopback interface.
        ---
        parameters:
        - name: hostname
            in: path
            type: string
            required: true
            description: device ip/hostname
        -name: intf_name
            in: body
            type: string
            required: true
            description: loopback interface name
        -name: ipv4
            in: body
            type: string
            required: false
            description: ipv4 address of the loopback interface
        -name: ipv4_mask
            in: body
            type: string
            required: false
            description: ipv4 subnet mask. if ipv4 parameter configured, this field will be mandatory
        -name: desc
            in: body
            type: string
            required: false
            description: under inerface description will be added
        -name: device-username
            in: body
            type: string
            required: false
            description: username to connect with device
        -name: device-password
            in: body
            type: string
            required: false
            description: password to connect with device
        - name: port
            in: body
            type: string
            required: false
            description: device ssh port
        - name: platform
            in: body
            type: string
            required: false
            description: device platform based on ncclient supports
        responses:
        200:
            description: when dry-run feature enabled, this will return
            properties:
                config:
                type: xml
                description: XML config of the interface
        201:
            description: if interface created succesfully
        400:
            description: when interface configuration is incorrect
        401:
            description: if device authentication failed
        404:
            description: when interface already deleted or not found on device.
        422:
            description: If socket is wrong .Any exception that cannot be handles
        """

        switch = session.get("dry-run", "false")

        # gets info from request
        data = set_config_serializer()

        nc = NCCHandler(hostname, data)
        logger.info(f"setting config on {hostname} ...")
        result = nc.edit_intf(switch)

        intf_name = data.get("intf_name")

        if isinstance(result, bool):
            logger.info(f"sending info back to user")
            return {
                "status": "suceess",
                "message": f"interface <Loopback{intf_name}> created",
            }, 201

        elif isinstance(result, str):
            logger.info(f"sending info back to user with dry-run feature")
            return Response(result, mimetype="text/xml")
        elif "must be signed integer" in f"{result}":
            logger.error(f"interface name is incorrect -> {intf_name}")
            return {"status": "failed", "message": f"{result}"}, 400
        elif "not open socket" in f"{result}":
            logger.error(f"port is not reachable on {hostname}")
            return {"status": "failed", "message": f"{result}"}, 422
        elif "AuthenticationException" in f"{result}":
            logger.error(f"authentication error for {hostname}")
            return {
                "status": "failed",
                "message": f"{hostname} authentication failed. Please check .env file or ENV",
            }, 401
        elif "is not a valid value" in f"{result}":
            logger.error(
                f'parameters are not valid -> {data.get("ipv4", "")} | ipv4_mask{data.get("ipv4_mask", "")}'
            )
            return {
                "status": "failed",
                "message": f'check your configuration. it is not valid. ipv4: {data.get("ipv4", "")} | ipv4_mask{data.get("ipv4_mask", "")}',
            }, 400
        logger.critical(f"{result}")
        return {"status": "failed", "message": f"{result}"}, 400
