import logging

from flask_restful import Resource
from flask import session
from flask_jwt_extended import jwt_required

from src.serializers.feature_serial import feature_serializer

logger = logging.getLogger("app.feature")


class Feature(Resource):
    @jwt_required()
    def patch(self):
        """
        on/off dry-run feature
        ---
        parameters:
        - name: dry-run
            in: body
            type: string
            required: true
            description: switches the dry-run feature. options are "true" or "false"
        responses:
        200:
            description: feature switched and result is in response.
        400:
            description: when payload is empty or other values configured.
        """

        data = feature_serializer()
        switch = data.get("dry-run")

        if switch == "false" or switch == "true":
            logger.info(f"dry-run feature sets to <{switch}>")
            session["dry-run"] = switch
            logger.info(f"session updated to {session}")
            return {
                "status": "success",
                "message": f"feature updated to <{switch}>",
            }, 200