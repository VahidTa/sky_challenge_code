from flask import jsonify
from flask_restful import Resource
from flask_jwt_extended import create_access_token, jwt_required, unset_jwt_cookies

from src.serializers.user_serial import user_serializer


class UserLogin(Resource):
    def post(self):
        """assign token to user for authentication"""
        data = user_serializer()
        username = data.get("username")

        if username == "admin":
            access_token = create_access_token(identity=username)

            return {"access_token": access_token}, 200
        return {"status": "failed", "message": f"user {username} not valid"}, 401


class UserLogout(Resource):
    @jwt_required()
    def post(self):
        """logout user from the app"""
        response = jsonify({"msg": "logout successful"})
        unset_jwt_cookies(response)
        return response
