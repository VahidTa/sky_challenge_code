from flask_restful import reqparse


def query_serializer():
    parser = reqparse.RequestParser()

    parser.add_argument(
        "hostname",
        type=str,
        required=True,
        help="this field cannot be blank. hostname of device",
    )

    return parser.parse_args()
