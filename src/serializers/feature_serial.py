from flask_restful import reqparse


def feature_serializer():
    parser = reqparse.RequestParser()

    parser.add_argument(
        "dry-run",
        type=str,
        required=True,
        help="this field cannot be blank. Bad choice: {error_msg}",
        default="false",
        choices=("true", "false"),
    )

    return parser.parse_args()
