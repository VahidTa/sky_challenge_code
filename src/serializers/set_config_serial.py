import os
from flask_restful import reqparse


def set_config_serializer():

    parser = reqparse.RequestParser()
    
    parser.add_argument(
        "intf_name",
        type=str,
        required=True,
        help="this field cannot be blank. plese enter loopback interface name to configure",
    )

    parser.add_argument(
        "ipv4",
        type=str,
        required=False,
        help="loopback ipv4 address",
    )

    parser.add_argument(
        "ipv4_mask",
        type=str,
        required=False,
        help="loopback ipv4 subnet mask",
    )

    parser.add_argument(
        "desc",
        type=str,
        required=False,
        help="loopback description",
    )

    parser.add_argument(
        "port", type=int, required=False, help="device port", default=830
    )

    parser.add_argument(
        "device-username",
        type=str,
        required=False,
        help="device username",
        default=os.environ.get("DEVICE_USERNAME"),
    )

    parser.add_argument(
        "device-password",
        type=str,
        required=False,
        help="device password",
        default=os.environ.get("DEVICE_PASSWORD"),
    )

    parser.add_argument(
        "platform",
        type=str,
        required=False,
        help="platform that ncclient supports",
        default="iosxe",
    )

    return parser.parse_args()
