import os
from flask_restful import reqparse


def del_config_serializer():

    parser = reqparse.RequestParser()

    parser.add_argument(
        "intf_name",
        required=True,
        help="this field cannot be blank. please enter loopback interface name to delete",
    )

    parser.add_argument(
        "port", type=int, required=False, help="device port", default=830
    )

    parser.add_argument(
        "device-username",
        required=False,
        help="device username",
        default=os.environ.get("DEVICE_USERNAME"),
    )

    parser.add_argument(
        "device-password",
        required=False,
        help="device password",
        default=os.environ.get("DEVICE_PASSWORD"),
    )

    parser.add_argument(
        "platform",
        required=False,
        help="platform that ncclient supports",
        default="iosxe",
    )

    return parser.parse_args()
