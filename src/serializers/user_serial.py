from flask_restful import reqparse


def user_serializer():
    parser = reqparse.RequestParser()

    parser.add_argument("username", required=True, help="this field cannot be blank")

    return parser.parse_args()
