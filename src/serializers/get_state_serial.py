import os
from flask_restful import reqparse


def get_state_serializer():

    parser = reqparse.RequestParser()

    parser.add_argument(
        "port", type=int, required=False, help="device port", default=22
    )

    parser.add_argument(
        "device-username",
        type=str,
        required=False,
        help="device username",
        default=os.environ.get("DEVICE_USERNAME"),
    )

    parser.add_argument(
        "device-password",
        type=str,
        required=False,
        help="device password",
        default=os.environ.get("DEVICE_PASSWORD"),
    )

    parser.add_argument(
        "platform",
        type=str,
        required=False,
        help="platform that ncclient supports",
        default="cisco_ios",
    )

    return parser.parse_args()
