import sys
import os

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import unittest
import app
import json

from flask_jwt_extended import create_access_token

class Sky01CreateTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app
        self.app.config["TESTING"] = True
        self.app.config["DEBUG"] = True
        self.client = self.app.test_client()
        self.ip = "10.1.181.80"
        self.headers = {"Content-Type": "application/json"}
    
    @property
    def _login_helper(self):
        self.payload = {"username": "admin"}
        response = self.client.post(
            f"/login",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        data = response.get_json()
        token = data.get('access_token')
        self.headers['Authorization'] = f'Bearer {token}'

    def test_create_loopback_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "3"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 201)

    def test_create_loopback_with_ip_set_config(self):
        self._login_helper
        self.payload = {
            "intf_name": "2",
            "ipv4": "2.2.2.2",
            "ipv4_mask": "255.255.255.255",
        }
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 201)

    def test_create_loopback_with_ip_and_desc_set_config(self):
        self._login_helper
        self.payload = {
            "intf_name": "2",
            "ipv4": "2.2.2.2",
            "ipv4_mask": "255.255.255.255",
            "desc": "new ip",
        }
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 201)

    def test_create_loopback_with_ip_and_no_mask_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "2", "ipv4": "2.2.2.2"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_wrong_device_ip_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.post(
            f"/device/1.1.1.1/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 422)

    def test_wrong_device_credential_set_config(self):
        self._login_helper
        self.payload = {
            "intf_name": "2",
            "device-username": "unittest",
            "device-password": "unittest",
            "desc": "new ip",
        }
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 401)


    def test_wrong_uri_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/1",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 404)

    def test_string_interface_name_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "vahid"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_float_interface_name_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "1.1"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_unsigned_interface_name_set_config(self):
        self._login_helper
        self.payload = {"intf_name": "-1"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_empty_body_set_config(self):
        self._login_helper
        self.payload = {}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)




class Sky02GetInfoTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app
        self.app.config["TESTING"] = True
        self.app.config["DEBUG"] = True
        self.client = self.app.test_client()
        self.ip = "10.1.181.80"
        self.headers = {"Content-Type": "application/json"}

    def test_success_get_state(self):
        response = self.client.get(f"/device/{self.ip}/get-state/")
        self.assertEqual(response.status_code, 200)

    def test_success_query_get_state(self):
        response = self.client.get(f"/device/{self.ip}/get-state/?port=22")
        self.assertEqual(response.status_code, 200)

    def test_wrong_uri_get_state(self):
        response = self.client.get(f"/device/{self.ip}/get-state/1")
        self.assertEqual(response.status_code, 404)

    def test_wrong_device_ip_get_state(self):
        response = self.client.get(f"/device/1.1.1.1/get-state/")
        self.assertEqual(response.status_code, 422)

    def test_wrong_query_get_state(self):
        response = self.client.get(f"/device/{self.ip}/get-state/?unittest=test")
        self.assertEqual(response.status_code, 200)


class Sky03DeleteTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app
        self.app.config["TESTING"] = True
        self.app.config["DEBUG"] = True
        self.client = self.app.test_client()
        self.ip = "10.1.181.80"
        self.headers = {"Content-Type": "application/json"}
    
    @property
    def _login_helper(self):
        self.payload = {"username": "admin"}
        response = self.client.post(
            f"/login",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        data = response.get_json()
        token = data.get('access_token')
        self.headers['Authorization'] = f'Bearer {token}'

    def test_success_delete_config(self):
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.delete(
            f"/device/{self.ip}/delete-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_wrong_uri_delete_config(self):
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.delete(
            f"/devicevahid/{self.ip}/delete-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 404)

    def test_wrong_device_ip_delete_config(self):
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.delete(
            f"/device/1.1.1.1/delete-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 422)

    def test_empty_body_delete_config(self):
        self._login_helper
        self.payload = {}
        response = self.client.delete(
            f"/device/{self.ip}/delete-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)


class Sky04FeatureTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app
        self.app.config["TESTING"] = True
        self.app.config["DEBUG"] = True
        self.app.config['SECRET_KEY'] = os.environ.get("SECRET_KEY")
        self.client = self.app.test_client()
        self.headers = {"Content-Type": "application/json"}
        self.ip = '10.1.181.80'
    
    @property
    def _login_helper(self):
        self.payload = {"username": "admin"}
        response = self.client.post(
            f"/login",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        data = response.get_json()
        token = data.get('access_token')
        self.headers['Authorization'] = f'Bearer {token}'
    
    def test_success01_feature(self):
        self._login_helper
        self.payload = {"dry-run": "true"}
        response = self.client.patch("/device/feature", data=json.dumps(self.payload), headers=self.headers)
        self.assertEqual(response.status_code, 200)

    def test_success02_set_config_feature(self):
        self.test_success01_feature()
        self._login_helper
        self.payload = {"intf_name": "3"}
        response = self.client.post(
            f"/device/{self.ip}/set-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_success03_delete_config_feature(self):
        self.test_success01_feature()
        self._login_helper
        self.payload = {"intf_name": "2"}
        response = self.client.delete(
            f"/device/{self.ip}/delete-config/",
            data=json.dumps(self.payload),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_wrong_uri_feature(self):
        self._login_helper
        self.payload = {"dry-run": "true"}
        response = self.client.patch("/device/features", data=json.dumps(self.payload), headers=self.headers,)
        self.assertEqual(response.status_code, 404)

    def test_wrong_body_feature(self):
        self._login_helper
        self.payload = {"dry-run": "vahid"}
        response = self.client.patch("/device/feature", data=json.dumps(self.payload), headers=self.headers,)
        self.assertEqual(response.status_code, 400)

    def test_empty_body_feature(self):
        self._login_helper
        self.payload = {}
        response = self.client.patch("/device/feature", data=json.dumps(self.payload), headers=self.headers,)
        self.assertEqual(response.status_code, 400)

    def test_list_body_feature(self):
        self._login_helper
        self.payload = {"dry-run": ["vahid"]}
        response = self.client.patch("/device/feature", data=json.dumps(self.payload), headers=self.headers,)
        self.assertEqual(response.status_code, 400)


if __name__ == "__main__":
    unittest.main(verbosity=2, warnings=False)
