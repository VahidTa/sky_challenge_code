import os

from flask import Flask
from flask_restful import Api
from dotenv import load_dotenv
from flask_jwt_extended import JWTManager

from src.resources.get_state import GetState
from src.resources.set_config import SetConfig
from src.resources.del_state import DeleteConfig
from src.resources.feature import Feature
from src.resources.user import UserLogin, UserLogout
from src.resources.query import Query
from src.utils.log import app_logger
from src.handlers.db import mongodb_client

load_dotenv()
logger = app_logger()

logger.info("app is starting ...")
app = Flask(__name__)
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")
app.config["MONGO_URI"] = os.environ.get("MONGO_URI")
app.config['PROPAGATE_EXCEPTIONS'] = True
api = Api(app, catch_all_404s=True)
mongodb_client.init_app(app)

# JWT token 
jwt = JWTManager(app)
@jwt.invalid_token_loader
@jwt.unauthorized_loader
def missing_token_callback(error):
    return {"status": "failed", "message": 'please login. token cannot be empty or null'}, 401

# Resources
api.add_resource(GetState, "/device/<string:hostname>/get-state/")
api.add_resource(SetConfig, "/device/<string:hostname>/set-config/")
api.add_resource(DeleteConfig, "/device/<string:hostname>/delete-config/")
api.add_resource(Feature, "/device/feature")
api.add_resource(UserLogin, "/login")
api.add_resource(UserLogout, "/logout")
api.add_resource(Query, "/query")

if __name__ == "__main__":
    app.run()
