FROM python:3.8

# Setting variables
ENV APP_HOME=/var/www
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

COPY . $APP_HOME
WORKDIR $APP_HOME

# install python packages
RUN pip install -r $APP_HOME/requirements.txt

# installing nginx
RUN apt update
RUN apt install -y nginx
RUN rm /etc/nginx/sites-enabled/default
COPY ./etc/nginx_single_docker.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

CMD [ "./etc/run.sh" ]
